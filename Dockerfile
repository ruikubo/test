FROM centos:7

RUN yum -y --setopt=tsflags=nodocs update && \
    yum -y --setopt=tsflags=nodocs install rsyslog && \
    yum clean all

# enable forwarding rule
#RUN sed -i "s/^#\$ActionQueue/\$ActionQueue/g" /etc/rsyslog.conf && \
#    sed -i "s/^#\$ActionResumeRetryCount/\$ActionResumeRetryCount/" /etc/rsyslog.conf

ENTRYPOINT ["rsyslogd", "-n"]